#include "matrice.h"

//Creer les matrice test du 4.1.
float **initMatTest(float **matrice, float *matriceTest, int n){     

    int i,j,k = 0;

    for (i=0; i<n; i++){

        for (j=0; j<n; j++){

            matrice[i][j] = matriceTest[k];

            k++;
        
        }
    }

    return (matrice);

}

//creer la matrice test du 4.2 A5
float ** initA5(float**matrice, int n){
    
    int i;

    for (i=0; i<n; i++){            
                            
        matrice[0][i] = pow(2,1-(i+1));
        matrice[i][0] = pow(2,1-(i+1));
        matrice[i][i] = 1;
    
    }

    return (matrice);

}

//creer la matrice test du 4.2 A6
float ** initA6(float**matrice, int n){  

    int i,j;

    for(i=0; i<n; i++){

        for(j=0; j<n; j++){

            if (i==j) {

                matrice[i][j] = 3;
            
            }
            else if((i<(n-1)) && (j==i+1)) {
            
                matrice [i][j] = -1;
            
            } 
            else if((i>0) && (j== i-1)){
            
                matrice[i][j] = -2;
            
            }

        }
    }

    return (matrice);

}