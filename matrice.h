#include <stdio.h>
#include <stdlib.h>
#include <math.h>


float ** CreerMatrice(int n){
       
    int i;
    float ** matrice = malloc(n*sizeof(float*));

    if (matrice == NULL){

       exit (-1);
       
    }

    for(i=0; i<n; i++){

        matrice[i] = malloc(n*sizeof(float));

        if (matrice[i] == NULL){

            free(matrice);

            exit(-2);

        }
        
    }
    
    return (matrice);

}


void libereMat(float** Mat, int n){

    if (Mat == NULL) {

        exit(-1);

    }
    
    for (int i=0; i<n; i++){

        free(Mat[i]);

    }

    free(Mat);
    Mat = NULL;

}