#include "Gauss.h"
#define couleur(param) printf("\033[%sm",param)

void affMat(float ** matrice, int tailleMat){

    int i,j;
  
    printf("\n\n");

    for (i=0; i<tailleMat; i++){

        for (j=0; j<tailleMat; j++){

            if (j%2 == 0) {couleur("34");}

            else {couleur("35");}
        
            printf(" %9f ", matrice[i][j]);
        
            couleur("0");

            if (j != tailleMat-1) {printf(",");}
        
        }
        
        printf("\n\n");

    }
}