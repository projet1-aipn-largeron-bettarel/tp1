#include <stdio.h>
#include <stdlib.h>
#define couleur(param) printf("\033[%sm",param)

float ** CreerMatrice(int n){
       
    int i;
    float ** matrice = malloc(n*sizeof(float*));

    if (matrice == NULL){

       exit (-1);
       
    }

    for(i=0; i<n; i++){

        matrice[i] = malloc(n*sizeof(float));

        if (matrice[i]==NULL){

            free(matrice);

            exit (-2);
        }
        
    }
    
    return (matrice);
}

float **initMatTest(float **matrice, float *matriceTest, int n){     

        int i,j,k = 0;

        for (i=0; i<n; i++){

            for (j=0; j<n; j++){

                matrice [i][j] = matriceTest[k];

                k++;
            }
        }
        return (matrice);
    }

void libereMat(float** Mat, int n){

    if (Mat == NULL) {

        exit(-1);
    }

    for (int i = 0; i < n; i++) {

        free(Mat[i]);
    }

    free(Mat);
    Mat = NULL;
}

void affMat(float ** matrice, int tailleMat){

    int i,j;
  
    printf("\n\n");

    for (i=0;i<tailleMat;i++){

        for (j=0;j<tailleMat;j++){
	    
	    if (j%2==0)
	    {
         	couleur("34");
        }
        else 
        {
            couleur("35");
        }

        printf(" %9f ",matrice[i][j]);

        couleur("0");

        if (j!= tailleMat-1)
           	{
               	printf(",");
          	}
        
        
        }
        printf("\n\n");
    }
    
}

int main()
{
    /*
    30 : Noir, 31 : Rouge, 32Vert, 33Jaune, 34Bleu, 35Magenta, 36Cyan, 37Blanc
    */
    
    couleur("34"); //met la couleur du texte en bleu.
    printf("texte affiché en bleu\n");
    couleur("0"); //réinitialise le système de couleur.

    couleur("35");
    printf("texte en mauve\n");
    couleur("0");

    float **A1 = CreerMatrice(3);
    float testA1[9]={3,0,4, 7,4,2, -1,1,2};  
    initMatTest (A1,testA1,3);
    affMat((A1),3); 
    libereMat(A1,3);
}